// ТЕОРІЯ
/*
1.Рядок в JS створюється за допомогою одинарних лапок ,подвійних лапок,та зворотніх лапок. А також за допомогою конструктора String(), який генерує значення в примітивний рядок.

2. Різниці між одинарними та подвійними лапками немає. Зворотні лапки мають додатковий функціонал .

3. Порівняння рядків відбувається за алфавітним порядком.Порівняння здійснюються за допомогою операторів порівняння та вираховується між ними булевий тип.

4. Функція Date.now() повертає поточну дату та час у форматі часових мілісекунд, що пройшли з 1 січня 1970 року до моменту виклику функції.

5. Чим відрізняється Date.now() від new Date()?
Date.now() повертає кількість секунд які минули від початку першого числа першого місяця поточного року.
new Date() нам повертає поточну дату .
*/



// ПРАКТИКА

// 1.


function isPalindrome(str) {
    
    str = str.toLowerCase().replace(/[^a-zа-я]/g, '');
    
    let palindrome = str === str.split('').reverse().join('');
    
    if (palindrome) {
        console.log("Є паліндромом.");
    } else {
        console.log("Рядок не є паліндромом.");
    }
    
    return palindrome;
}

// 2.



function checkStringLength(str, maxLength) {
    return str.length <= maxLength;
}

console.log(checkStringLength('checked string', 20)); 
console.log(checkStringLength('checked string', 10)); 


// 3.

let born = prompt("Введіть будьласка рік місяць та день народження");
let date2 = new Date();
const rev = born.split('.').map(Number).reverse();
function birthCalculate(birth){
    birth = new Date(birth);
    return birth = parseInt((date2 - birth) / (1000 * 60 * 60 * 24 * 365.25));
}
console.log(date2);
console.log(rev);
alert(birthCalculate(rev));


